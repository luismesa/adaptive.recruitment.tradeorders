﻿// *****************************************************************
// *****************************************************************
// *****************************************************************
// PLEASE DO NOT MODIFY THIS FILE - IT IS NOT A PART OF THE EXERCISE
// *****************************************************************
// *****************************************************************
// *****************************************************************
using System;

namespace Adaptive.Recruitment.TradeOrders.Contracts
{
    public interface ITradeOrder
    {
        event EventHandler<TradeFailureEventArgs> TradeFailure;
        event EventHandler TradeSuccess;

        void OnPriceTick(string stockSymbol, decimal price);
        void Cancel();
    }
}
