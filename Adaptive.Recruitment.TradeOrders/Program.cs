﻿using System;
using Adaptive.Recruitment.TradeOrders.System;

namespace Adaptive.Recruitment.TradeOrders
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var tradeBooker = new TradeBooker();
            var orderManagementSystem = new OrderManagementSystem();


            // TODO - register orders
            // BUY 50 shares in Apple(stock symbol: AAPL) at a maximum price of $100.
            // SELL 200 shares in Microsoft(stock symbol: MSFT) at a minimum price of $50.




            orderManagementSystem.Run();

            Console.WriteLine("Press 'C' to cancel all orders or 'Enter' to quit.");
            var info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key == ConsoleKey.C)
                {
                    orderManagementSystem.CancelAllOrders();
                }
                info = Console.ReadKey(true);
            }
        }
    }
}
