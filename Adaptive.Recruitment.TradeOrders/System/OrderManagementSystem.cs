﻿// *****************************************************************
// *****************************************************************
// *****************************************************************
// PLEASE DO NOT MODIFY THIS FILE - IT IS NOT A PART OF THE EXERCISE
// *****************************************************************
// *****************************************************************
// *****************************************************************
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Adaptive.Recruitment.TradeOrders.Contracts;

namespace Adaptive.Recruitment.TradeOrders.System
{
    internal class OrderManagementSystem
    {
        private readonly ConcurrentBag<ITradeOrder> _registeredOrders = new ConcurrentBag<ITradeOrder>();

        private readonly Tuple<string, decimal>[] _supportedSymbols =
        {
            new Tuple<string, decimal>("AAPL", 90m),
            new Tuple<string, decimal>("MSFT", 45m),
            new Tuple<string, decimal>("GOOG", 700m),
            new Tuple<string, decimal>("AMZN", 550m),
            new Tuple<string, decimal>("NFLX", 100m),
            new Tuple<string, decimal>("FB", 100m),
        };

        public void RegisterOrder(ITradeOrder tradeOrder)
        {
            tradeOrder.TradeSuccess += (s, e) => Console.WriteLine("*** Trade Success from order '{0}' ***", tradeOrder);
            tradeOrder.TradeFailure += (s, e) => Console.WriteLine("*** Trade Failure from order '{0}' - '{1}' ***", tradeOrder, e.FailureMessage);
            _registeredOrders.Add(tradeOrder);
        }

        public Task Run()
        {
            Console.WriteLine("Starting...");

            var tasks = Enumerable.Range(0, 100).Select(_ =>
             CreateUpdatePriceEverySecondLongRunningTask()).ToArray();

            Console.WriteLine("Started.");

            return Task.WhenAll(tasks);
        }

        private Task CreateUpdatePriceEverySecondLongRunningTask()
        {
            return Task.Factory.StartNew(() =>
            UpdateRandomSymbolPriceAndNotifyRegisteredOrdersEverySecond(), TaskCreationOptions.LongRunning);
        }

        private void UpdateRandomSymbolPriceAndNotifyRegisteredOrdersEverySecond()
        {
            var random = new Random();
            while (true)
            {
                var randomSymbolForPriceUpdate = _supportedSymbols[random.Next(_supportedSymbols.Length)];

                var newPrice = randomSymbolForPriceUpdate.Item2 + random.Next(10);

                foreach (var registeredOrder in _registeredOrders)
                {
                    var callTime = DateTime.UtcNow;
                    registeredOrder.OnPriceTick(randomSymbolForPriceUpdate.Item1, newPrice);
                    if (DateTime.UtcNow > callTime.AddSeconds(1))
                    {
                        Console.WriteLine("WARNING - OnPriceTick to order '{0}' took over 1 second.", registeredOrder);
                    }
                }

                Thread.Sleep(1000);
            }
        }

        public void CancelAllOrders()
        {
            if (_registeredOrders.Any())
            {
                Console.WriteLine("Cancelling {0} orders.", _registeredOrders.Count);
                foreach (var registeredOrder in _registeredOrders)
                {
                    registeredOrder.Cancel();
                }
            }
            else
            {
                Console.WriteLine("No running orders.");
            }
        }
    }
}