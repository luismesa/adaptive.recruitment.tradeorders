﻿using Adaptive.Recruitment.TradeOrders.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptive.Recruitment.TradeOrders.TradeOrders
{
    public class SellOver50MicrosoftTradeOrder : TradeOrderBase
    {
        private const string MicrosoftStockSymbol = "MSFT";
        private const decimal MinimumPriceInDollars = 50;
        private const int StockVolumeToSell = 200;

        public SellOver50MicrosoftTradeOrder(ITradeBooker tradeBooker)
            :base(tradeBooker)
        {

        }

        protected override void PlaceTransaction(string stockSymbol, decimal price)
        {
            _TradeBooker.Sell(stockSymbol, StockVolumeToSell, price);
        }

        protected override bool StockAndPriceAreEligible(string stockSymbol, decimal price)
        {
            return stockSymbol == MicrosoftStockSymbol && price > 50;
        }
    }
}
