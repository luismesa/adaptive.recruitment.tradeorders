﻿using Adaptive.Recruitment.TradeOrders.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptive.Recruitment.TradeOrders.TradeOrders
{
    public class BuyUnder100AppleTradeOrder : TradeOrderBase
    {
        private const string AppleStockSymbol = "AAPL";
        private const decimal MaximumPriceInDollars = 100;
        private const int StockVolumeToBuy = 50;

        public BuyUnder100AppleTradeOrder(ITradeBooker tradeBooker)
            :base(tradeBooker)
        {
        }

        protected override void PlaceTransaction(string stockSymbol, decimal price)
        {
            _TradeBooker.Buy(stockSymbol, StockVolumeToBuy, price);
        }

        protected override bool StockAndPriceAreEligible(string stockSymbol, decimal price)
        {
            return stockSymbol == AppleStockSymbol && price < MaximumPriceInDollars;
        }
    }
}
