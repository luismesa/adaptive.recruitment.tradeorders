﻿using Adaptive.Recruitment.TradeOrders.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptive.Recruitment.TradeOrders.TradeOrders
{
    public abstract class TradeOrderBase : ITradeOrder
    {

        private object PlacingTransactionLock = new object();
        private bool TransactionPlaced = false;
        private bool OrderCancelled = false;

        protected bool TransactionNotPlacedYetAndOrderNotCancelled
        {
            get
            {
                return !TransactionPlaced && !OrderCancelled;
            }
        }

        protected readonly ITradeBooker _TradeBooker;

        public TradeOrderBase(ITradeBooker tradeBooker)
        {
            _TradeBooker = tradeBooker;
            TradeSuccess = (s, e) => { return; };
            TradeFailure = (s, e) => { return; };
        }

        public event EventHandler<TradeFailureEventArgs> TradeFailure;
        public event EventHandler TradeSuccess;

        public void OnPriceTick(string stockSymbol, decimal price)
        {
            if (!StockAndPriceAreEligible(stockSymbol, price))
            {
                return;
            }

            if (TransactionNotPlacedYetAndOrderNotCancelled)
            {
                lock (PlacingTransactionLock)
                {
                    if (TransactionNotPlacedYetAndOrderNotCancelled)
                    {
                        try
                        {
                            PlaceTransaction(stockSymbol, price);
                            TradeSuccess(this, EventArgs.Empty);
                        }
                        catch (Exception ex)
                        {
                            var errorMessage = string.Format("Failed placing transaction for '{0}' with price '{1}'. ex: '{2}'", stockSymbol, price, ex.ToString());
                            TradeFailure(this, new TradeFailureEventArgs(errorMessage));
                        }

                        TransactionPlaced = true;
                    }
                }
            }
        }

        public void Cancel()
        {
            lock (PlacingTransactionLock)
            {
                OrderCancelled = true;
            }
        }

        protected abstract void PlaceTransaction(string stockSymbol, decimal price);

        protected abstract bool StockAndPriceAreEligible(string stockSymbol, decimal price);

    }
}
