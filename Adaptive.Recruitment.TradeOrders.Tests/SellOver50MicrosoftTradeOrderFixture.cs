﻿using Adaptive.Recruitment.TradeOrders.Contracts;
using Adaptive.Recruitment.TradeOrders.TradeOrders;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMock;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Adaptive.Recruitment.TradeOrders.Tests
{
    [TestClass]
    public class SellOver50MicrosoftTradeOrderFixture
    {
        private const string MicrosoftStockSymbol = "MSFT";

        [TestMethod]
        public void GivenMicrosoftPriceChangesToEligiblePrice_WhenOrderIsNotified_Then200SharesAreSold()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 90m;

            var expectedStockPrice = eligiblePrice;
            var expectedVolume = 200;
            var expectedStockSymbol = MicrosoftStockSymbol;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Sell(null, 0, 0))
                .With(expectedStockSymbol, expectedVolume, expectedStockPrice);

            // ACT
            var sut = new SellOver50MicrosoftTradeOrder(tradeBooker.MockObject);
            sut.OnPriceTick(MicrosoftStockSymbol, eligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void GivenMicrosoftPriceChangesToNotEligiblePrice_WhenOrderIsNotified_ThenNoSharesAreSold()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var notEligiblePrice = 40m;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .No
                .Method(x => x.Sell(null, 0, 0));

            // ACT
            var sut = new SellOver50MicrosoftTradeOrder(tradeBooker.MockObject);
            sut.OnPriceTick(MicrosoftStockSymbol, notEligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void GivenPriceOtherThanMicrosoftChangesToEligiblePrice_WhenOrderIsNotified_ThenNoSharesAreSold()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 80m;
            var otherThanMicrosoftStockSymbol = "Macrohard";

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .No
                .Method(x => x.Sell(null, 0, 0));

            // ACT
            var sut = new SellOver50MicrosoftTradeOrder(tradeBooker.MockObject);
            sut.OnPriceTick(otherThanMicrosoftStockSymbol, eligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void GivenMicrosoftPriceChangesToEligiblePrice_WhenOrderIsNotifiedTwice_ThenOnlyOneTransactionIsPlacedWithTheFirstPrice()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var firstEligiblePrice = 60m;
            var anotherEligiblePrice = 80m;

            var expectedStockSymbol = MicrosoftStockSymbol;
            var expectedStockPrice = firstEligiblePrice;
            var expectedVolume = 200;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Sell(null, 0, 0))
                .With(expectedStockSymbol, expectedVolume, expectedStockPrice);

            // ACT
            var sut = new SellOver50MicrosoftTradeOrder(tradeBooker.MockObject);
            sut.OnPriceTick(MicrosoftStockSymbol, firstEligiblePrice);
            sut.OnPriceTick(MicrosoftStockSymbol, anotherEligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public async Task GivenMicrosoftPriceChangesToEligiblePrice_WhenOrderIsNotifiedThreeTimesFromDifferentThreads_ThenOnlyOneTransactionIsPlaced()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var oneEligiblePrice = 90m;
            var anotherEligiblePrice = 80m;
            var yetAnotherEligiblePrice = 56m;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Sell(null, 0, 0))
                .WithAnyArguments();

            var tasks = new List<Task>();

            // ACT
            var sut = new SellOver50MicrosoftTradeOrder(tradeBooker.MockObject);
            tasks.Add(Task.Factory.StartNew(() => sut.OnPriceTick(MicrosoftStockSymbol, oneEligiblePrice)));
            tasks.Add(Task.Factory.StartNew(() => sut.OnPriceTick(MicrosoftStockSymbol, anotherEligiblePrice)));
            tasks.Add(Task.Factory.StartNew(() => sut.OnPriceTick(MicrosoftStockSymbol, yetAnotherEligiblePrice)));

            await Task.WhenAll(tasks);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void GivenMicrosoftPriceChangesToEligiblePriceAndOrderHasBeenCancelled_WhenOrderIsNotified_ThenNoSharesAreBought()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 80m;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .No
                .Method(x => x.Sell(null, 0, 0));

            // ACT
            var sut = new SellOver50MicrosoftTradeOrder(tradeBooker.MockObject);
            sut.Cancel();
            sut.OnPriceTick(MicrosoftStockSymbol, eligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }


        [TestMethod]
        public void GivenMicrosoftPriceChangesToEligiblePrice_WhenTransactionIsPlacedWithoutException_ThenTradeSuccessEventHandlerIsRaised()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 80m;

            var tradeSuccessRaiseCount = 0;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Sell(null, 0, 0))
                .WithAnyArguments();

            // ACT
            var sut = new SellOver50MicrosoftTradeOrder(tradeBooker.MockObject);
            sut.TradeFailure += (s, e) => { throw new AssertFailedException("Trade failures should not be raised."); };
            sut.TradeSuccess += (s, e) => { tradeSuccessRaiseCount++; };

            sut.OnPriceTick(MicrosoftStockSymbol, eligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
            tradeSuccessRaiseCount.Should().Be(1);
        }

        [TestMethod]
        public void GivenMicrosoftPriceChangesToEligiblePrice_WhenPlaceTransactionThrowsException_ThenTradeFailureEventHandlerIsRaised()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 80m;

            var tradeFailureRaiseCount = 0;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Sell(null, 0, 0))
                .Will(Throw.Exception(new Exception()));

            // ACT
            var sut = new SellOver50MicrosoftTradeOrder(tradeBooker.MockObject);
            sut.TradeSuccess += (s, e) => { throw new AssertFailedException("TradeSuccess should not be raised."); };
            sut.TradeFailure += (s, e) => { tradeFailureRaiseCount++; };

            sut.OnPriceTick(MicrosoftStockSymbol, eligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
            tradeFailureRaiseCount.Should().Be(1);
        }

        [TestMethod]
        public void GivenMicrosoftPriceChangesToEligiblePrice_WhenPlaceTransactionThrowsExceptionAndOrderIsNotifiedAgain_ThenTransactionIsNotRetried()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 80m;
            var anotherEligiblePrice = 90m;

            var tradeFailureRaiseCount = 0;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Sell(null, 0, 0))
                .Will(Throw.Exception(new Exception()));

            // ACT
            var sut = new SellOver50MicrosoftTradeOrder(tradeBooker.MockObject);
            sut.TradeSuccess += (s, e) => { throw new AssertFailedException("TradeSuccess should not be raised."); };
            sut.TradeFailure += (s, e) => { tradeFailureRaiseCount++; };

            sut.OnPriceTick(MicrosoftStockSymbol, eligiblePrice);
            sut.OnPriceTick(MicrosoftStockSymbol, anotherEligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
            tradeFailureRaiseCount.Should().Be(1);
        }
    }
}
