﻿using Adaptive.Recruitment.TradeOrders.Contracts;
using Adaptive.Recruitment.TradeOrders.TradeOrders;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMock;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using System;

namespace Adaptive.Recruitment.TradeOrders.Tests
{
    [TestClass]
    public class BuyUnder100AppleTradeOrderFixture
    {
        private const string AppleStockSymbol = "AAPL";

        [TestMethod]
        public void GivenApplePriceChangesToEligiblePrice_WhenOrderIsNotified_Then50SharesAreBought()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 90m;

            var expectedStockPrice = eligiblePrice;
            var expectedVolume = 50;
            var expectedStockSymbol = AppleStockSymbol;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Buy(null, 0, 0))
                .With(expectedStockSymbol, expectedVolume, expectedStockPrice);
            
            // ACT
            var sut = new BuyUnder100AppleTradeOrder(tradeBooker.MockObject);
            sut.OnPriceTick(AppleStockSymbol, eligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void GivenApplePriceChangesToNotEligiblePrice_WhenOrderIsNotified_ThenNoSharesAreBought()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var notEligiblePrice = 110m;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .No
                .Method(x => x.Buy(null, 0, 0));

            // ACT
            var sut = new BuyUnder100AppleTradeOrder(tradeBooker.MockObject);
            sut.OnPriceTick(AppleStockSymbol, notEligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void GivenPriceOtherThanAppleChangesToEligiblePrice_WhenOrderIsNotified_ThenNoSharesAreBought()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 80m;

            var otherThanAppleStockSymbol = "PEAR";

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .No
                .Method(x => x.Buy(null, 0, 0));

            // ACT
            var sut = new BuyUnder100AppleTradeOrder(tradeBooker.MockObject);
            sut.OnPriceTick(otherThanAppleStockSymbol, eligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void GivenApplePriceChangesToEligiblePrice_WhenOrderIsNotifiedTwice_ThenOnlyOneTransactionIsPlacedWithTheFirstPrice()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var firstEligiblePrice = 60m;
            var anotherEligiblePrice = 80m;

            var expectedStockSymbol = AppleStockSymbol;
            var expectedStockPrice = firstEligiblePrice;
            var expectedVolume = 50;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Buy(null, 0, 0))
                .With(expectedStockSymbol, expectedVolume, expectedStockPrice) ;

            // ACT
            var sut = new BuyUnder100AppleTradeOrder(tradeBooker.MockObject);
            sut.OnPriceTick(AppleStockSymbol, firstEligiblePrice);
            sut.OnPriceTick(AppleStockSymbol, anotherEligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public async Task GivenApplePriceChangesToEligiblePrice_WhenOrderIsNotifiedThreeTimesFromDifferentThreads_ThenOnlyOneTransactionIsPlaced()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var oneEligiblePrice = 90m;
            var anotherEligiblePrice = 80m;
            var yetAnotherEligiblePrice = 56m;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Buy(null, 0, 0))
                .WithAnyArguments();

            var tasks = new List<Task>();

            // ACT
            var sut = new BuyUnder100AppleTradeOrder(tradeBooker.MockObject);
            tasks.Add(Task.Factory.StartNew(() => sut.OnPriceTick(AppleStockSymbol, oneEligiblePrice)));
            tasks.Add(Task.Factory.StartNew(() => sut.OnPriceTick(AppleStockSymbol, anotherEligiblePrice)));
            tasks.Add(Task.Factory.StartNew(() => sut.OnPriceTick(AppleStockSymbol, yetAnotherEligiblePrice)));

            await Task.WhenAll(tasks);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void GivenApplePriceChangesToEligiblePriceAndOrderHasBeenCancelled_WhenOrderIsNotified_ThenNoSharesAreBought()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 80m;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .No
                .Method(x => x.Buy(null, 0, 0));

            // ACT
            var sut = new BuyUnder100AppleTradeOrder(tradeBooker.MockObject);
            sut.Cancel();
            sut.OnPriceTick(AppleStockSymbol, eligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void GivenApplePriceChangesToEligiblePrice_WhenTransactionIsPlacedWithoutException_ThenTradeSuccessEventHandlerIsRaised()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 80m;

            var tradeSuccessRaiseCount = 0;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Buy(null, 0, 0))
                .WithAnyArguments();

            // ACT
            var sut = new BuyUnder100AppleTradeOrder(tradeBooker.MockObject);
            sut.TradeFailure += (s, e) => { throw new AssertFailedException("TradeFailure should not be raised."); };
            sut.TradeSuccess += (s, e) => { tradeSuccessRaiseCount++; };

            sut.OnPriceTick(AppleStockSymbol, eligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
            tradeSuccessRaiseCount.Should().Be(1);
        }

        [TestMethod]
        public void GivenApplePriceChangesToEligiblePrice_WhenPlaceTransactionThrowsException_ThenTradeFailureEventHandlerIsRaised()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 80m;

            var tradeFailureRaiseCount = 0;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Buy(null, 0, 0))
                .Will(Throw.Exception(new Exception()));

            // ACT
            var sut = new BuyUnder100AppleTradeOrder(tradeBooker.MockObject);
            sut.TradeSuccess += (s, e) => { throw new AssertFailedException("TradeSuccess should not be raised."); };
            sut.TradeFailure += (s, e) => { tradeFailureRaiseCount++; };

            sut.OnPriceTick(AppleStockSymbol, eligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
            tradeFailureRaiseCount.Should().Be(1);
        }

        [TestMethod]
        public void GivenApplePriceChangesToEligiblePrice_WhenPlaceTransactionThrowsExceptionAndOrderIsNotifiedAgain_ThenTransactionIsNotRetried()
        {
            var mockFactory = new MockFactory();

            // ARRANGE
            var eligiblePrice = 80m;
            var anotherEligiblePrice = 90m;

            var tradeFailureRaiseCount = 0;

            var tradeBooker = mockFactory.CreateMock<ITradeBooker>();
            tradeBooker
                .Expects
                .One
                .Method(x => x.Buy(null, 0, 0))
                .Will(Throw.Exception(new Exception()));

            // ACT
            var sut = new BuyUnder100AppleTradeOrder(tradeBooker.MockObject);
            sut.TradeSuccess += (s, e) => { throw new AssertFailedException("TradeSuccess should not be raised."); };
            sut.TradeFailure += (s, e) => { tradeFailureRaiseCount++; };

            sut.OnPriceTick(AppleStockSymbol, eligiblePrice);
            sut.OnPriceTick(AppleStockSymbol, anotherEligiblePrice);

            // ASSERT
            mockFactory.VerifyAllExpectationsHaveBeenMet();
            tradeFailureRaiseCount.Should().Be(1);
        }
    }
}
