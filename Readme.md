**Implement stock trade orders**

You are to implement two orders to:
  - BUY 50 shares in Apple (stock symbol: AAPL) at a maximum price of $100.
  - SELL 200 shares in Microsoft (stock symbol: MSFT) at a minimum price of $50.

The orders will be loaded into an order management system which should not be modified.

The implementation should:

* Implement the `Adaptive.Recruitment.TradeOrders.Contracts.ITradeOrder` interface.
* Place a single BUY or SELL transaction via `Adaptive.Recruitment.TradeOrders.Contracts.ITradeBooker` for a received eligible price. Note:
  * If the call is successful then the trade was booked.
  * If an exception is thrown then the trade has failed and the order should not retry.
* Raise the `ITradeOrder.TradeSucess` event upon success.
* Raise the `ITradeOrder.TradeFailure` event upon failure.
* Implement the ITradeOrder.Cancel() method to allow the order to be cancelled externally.
* Be designed to run in a multi threaded environment (ITradeOrder.OnPriceTick(...) can be called from many threads simultaneously). 
* Be unit tested.
